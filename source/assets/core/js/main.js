let config = getEnv();

// Here we can set Moment library for the language we want, portuguese in this example
moment.locale('pt-br');

// Checking if we have the expected DOM Element to start app
if (document.getElementById('test-app')) {
    // VUE JS Instance
    var view = new Vue ({
        el: '#test-app',
        data: {

            // Page Main Behavior
            loadingArticle: true,

            // Page Data
            pageArticle: {},
            relatedArticles: [],

        },
        methods: {
            getArticleDetails: function(articleId) {
                return new Promise((resolve, reject) => {
                    axios.get(config.apiUrl + 'posts/'+ articleId).then((response) => {
                        this.pageArticle = response.data;
                        resolve();
                    });
                });
            },
            getArticleComments: function(articleId) {
                return new Promise((resolve, reject) => {
                    axios.get(config.apiUrl + 'comments/' + articleId).then((response) => {
                        this.pageArticle.comments = response.data;
                        resolve();
                    });
                });
            },
            getArticlePicture: function(articleId) {
                return new Promise((resolve, reject) => {
                    axios.get(config.apiUrl + 'photos/' + articleId).then((response) => {
                        this.pageArticle.photo = response.data;
                        resolve();
                    });
                });
            },
            getRelatedArticles: function() {
                return new Promise((resolve, reject) => {
                    axios.get(config.apiUrl + 'posts/').then((response) => {
                        this.relatedArticles = response.data.slice(0, 10); // We don't want a hundred now
                        resolve();
                    });
                });
            }
        },
        created: function() {
            // Let's pretend we have the ID of the article we're opening, from the previous flow which led us here.
            let ourArticle = 1

            // Gets Article Details and then fills up it's properties (unfortunately the mock doesn't serve us the best JSON structure)
            this.getArticleDetails(ourArticle).then(() => {
                this.getArticleComments(ourArticle);
                this.getArticlePicture(ourArticle).then(() => {
                    // Minimum info loaded, we can tell view already to start showing the parts we want
                    this.loadingArticle = false;
                });
            });

            this.getRelatedArticles().then(() => {
                if (window.$) {
                    $('.articles-slider').slick({
                        slidesPerRow: 3,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        autoplaySpeed: 2000,
                        autoplay: true,
                        pauseOnHover: true,
                        adaptiveHeight: true,
                        arrows: false,
                        centerMode: false,
                        dots: false,
                        responsive: [{
                            breakpoint: 649,
                            slidesPerRow: 1,
                            slidesToShow: 1,
                            settings: {
                                arrows: true
                            }
                        }]
                    });
                }
            });
        },
        mounted: function () {
            startCoreScripts();

            // Everything good to go, we can show page
            document.body.classList.remove('loading');

            // Just an example to show that we can use JQuery if necessary after having page mounted
            $(document).ready(function() {
                // JQuery stuff which may depend on DOM to be already rendered
            });
        }
    });
}
