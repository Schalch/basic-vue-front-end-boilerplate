var simpleRequest = function(method, route, data, type, progress) {
    return new Promise((resolve, reject) => {
        var token = sessionStorage.getItem('access_token'),
            token_type = sessionStorage.getItem('token_type');

        var m = method.toUpperCase();
        var r = new XMLHttpRequest();

        /*var requestBody = '';

        if (!token) {
            requestBody = requestBody+'grant_type=client_credentials';
            requestBody = requestBody+'&client_id=88f17ac1-e0a8-4b61-a13e-c4f1a2c369f8';
            requestBody = requestBody+'&client_secret='+token;
        }

        if (data) {
            for (var key in data) {
                requestBody = requestBody +'&'+ key +'='+ encodeURI(data[key]);
            }
        }*/

        if (data) {
            data = JSON.stringify(data);
        }

        r.open(m, route, true);

        // r.withCredentials = true;
        r.setRequestHeader('Content-type', 'application/json');
        r.setRequestHeader('Accept', 'application/json');

        /*r.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        r.setRequestHeader('Cache-Control', 'no-cache');*/

        if (token != null) {
            r.setRequestHeader('Authorization', (token_type || 'Bearer') + ' ' + token);
        }

        r.onreadystatechange = function() {
            if (this.readyState == 4) {

                if (this.status != 200) {

                    var errorResponse = JSON.parse(this.response);
                    console.error('Route:', this.responseURL);
                    console.error('Response:', errorResponse);

                    try {
                        reject(JSON.parse(r.responseText));
                    } catch (e) {
                        reject(r.responseText);
                    }

                } else {

                    if (type == 'blob') {
                        resolve(r.response);
                    } else {
                        try {
                            resolve(JSON.parse(r.responseText));
                        } catch (e) {
                            resolve(r.responseText);
                        }
                    }

                }

            }
        };
        r.send(data);
    });
};
