function getEnv() {
    return {
        apiUrl: '', // Here we can sent environment variables like the API main path for example
        frontUrl: '', // Also frontend-exclusive variables
    };
}
