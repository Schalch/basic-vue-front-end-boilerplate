// Gulp Dependencies
var gulp          = require('gulp'),
    order         = require('gulp-order'),
    print         = require('gulp-print').default,
    preprocess    = require('gulp-preprocess'),
    rename        = require('gulp-rename'),
    del           = require('del'),
    autoprefixer  = require('gulp-autoprefixer'),
    htmlmin       = require('gulp-htmlmin'),
    babel         = require('gulp-babel'),
    concat        = require('gulp-concat'),
    sourcemaps    = require('gulp-sourcemaps'),
    uglify        = require('gulp-uglify'),
    imagemin      = require('gulp-imagemin'),
    sass          = require('gulp-sass'),
    minifycss     = require('gulp-clean-css'),
    jsonminify    = require('gulp-jsonminify'),
    minifyInline  = require('gulp-minify-inline'),
    cache         = require('gulp-cache'),
    browserSync   = require('browser-sync').create();

// Path Vars
var sourcePath     = 'source/',
    buildPath      = 'temp/',
    sourceAssets   = sourcePath+'assets/',
    buildAssets    = buildPath+'assets/',
    finalBuildPath = 'build/';

var version  = String(new Date().getDate()) + String(new Date().getHours()) + String(new Date().getSeconds());
var watching = false;

// Watch - Updates Build running the specific task whenever a file/folder is changed
gulp.task('watch', function() {
    watching = true;

    // During Watch tasks, destination path shall be Build
    buildPath   = 'build/';
    buildAssets = buildPath+'assets/';

    browserSync.init({
        server: finalBuildPath
    });

    gulp.watch([sourceAssets+'core/**/*.scss', '!'+sourceAssets+'core/libs/**/*.scss'], ['create-core-dynamic-css']);
    gulp.watch([sourceAssets+'core/**/*.css', '!'+sourceAssets+'core/css/ui.css', '!'+sourceAssets+'core/libs/**/*.css'], ['create-core-dynamic-css']);
    gulp.watch([sourceAssets+'core/libs/**/*.css'], ['create-core-static-css']);
    gulp.watch([sourceAssets+'core/**/*.js', '!'+sourceAssets+'core/libs/**/*.js', '!'+sourceAssets+'core/js/essentials.js'], ['create-core-dynamic-js']);
    gulp.watch([sourceAssets+'core/js/essentials.js'],['create-core-head-js']);
    gulp.watch([sourceAssets+'core/fonts/*.*'],['move-all-core-fonts']);
    gulp.watch([sourceAssets+'core/libs/**/*.js'], ['create-core-static-js']);
    gulp.watch(sourceAssets+'core/img/**/*', ['compress-core-images']);
    gulp.watch(sourcePath+'**/*.html', ['move-all-components']);
    gulp.watch([sourcePath+'**/*.*',
        '!'+sourceAssets+'core/**/*.js',
        '!'+sourceAssets+'core/**/*.scss',
        '!'+sourceAssets+'core/**/*.css',
        '!'+sourceAssets+'core/img/*.*',
        '!'+sourceAssets+'core/libs/**/*.*',
        '!'+sourceAssets+'core/fonts/**/*.*',
        '!'+sourceAssets+'**/*.map',
        '!'+sourcePath+'**/*.html'],
        ['move-all-other-files']);
    gulp.watch([sourcePath+'**/*.*', '!'+sourceAssets+'core/**/*.scss', '!'+sourceAssets+'core/libs/**/*.scss'], function() {
        browserSync.reload();
    });
});

// Compiles SASS and sends it to source/css/ to be processed in CSS tasks later
gulp.task('core-sass', function () {
  return gulp.src(sourceAssets+'core/scss/ui.scss')
    //.pipe(sourcemaps.init())
    .pipe(sass({ includePaths:[sourceAssets+'core/scss/'] }).on('error', sass.logError))
    //.pipe(sourcemaps.write('/maps'))
    .pipe(gulp.dest(sourceAssets+'core/css/'));
});

// Creates dynamic CSS (all the CSS which is modified by development)
gulp.task('create-core-dynamic-css', ['core-sass'], function() {
  return gulp.src([sourceAssets+'core/**/*.css', '!'+sourceAssets+'core/fonts/**/*.css', '!'+sourceAssets+'core/libs/**/*.css'])
    .pipe(order([
    'ui.css',
    '**/*.css'
     ], {base: sourceAssets}))
    .pipe(sourcemaps.init())
    .pipe(concat('core-dynamic.css'))
    .pipe(autoprefixer('last 2 versions'))
    .pipe(minifycss())
    .pipe(rename({suffix:'.min'}))
    .pipe(sourcemaps.write('/maps'))
    .pipe(gulp.dest(buildAssets+'css/'))
});

// Creates static CSS (Vendors which are never directly modified)
gulp.task('create-core-static-css', function() {
  return gulp.src([sourceAssets+'core/fonts/**/*.css', sourceAssets+'core/libs/**/*.css', '!'+sourceAssets+'core/css/**/*.css'])
    .pipe(order([
    'fonts/*.css',
    'libs/foundation/*.css',
    '**/*.css'
     ], {base: sourceAssets}))
    .pipe(sourcemaps.init())
    .pipe(concat('core-static.css'))
    .pipe(autoprefixer('last 2 versions'))
    .pipe(rename({suffix:'.min'}))
    .pipe(minifycss({
        level: {
            1: {
                specialComments: 0 // Removes block comments from vendors.
            }
        }
    }))
    .pipe(sourcemaps.write('/maps'))
    .pipe(gulp.dest(buildAssets+'css/'))
});

// Move todos os arquivos de fontes do Core para a pasta de fonts da Build
gulp.task('move-all-core-fonts', function() {
    return gulp.src([sourceAssets+'core/fonts/**/*.*', '!'+sourceAssets+'core/fonts/**/*.css'])
    .pipe(gulp.dest(buildAssets+'fonts/'))
});

// Cria o bundle de JS essencial (head) do Core
gulp.task('create-core-head-js', function() {
  return gulp.src(sourceAssets+'core/js/essentials.js')
    .pipe(babel())
    .on('error', console.error.bind(console))
    .pipe(sourcemaps.init())
    .pipe(concat('core-head.js'))
    .pipe(rename({suffix:'.min'}))
    .pipe(uglify())
    .pipe(sourcemaps.write('/maps'))
    .pipe(gulp.dest(buildAssets+'js/'))
});

// Cria o bundle de JS dinâmico do Core
gulp.task('create-core-dynamic-js', ['create-core-head-js'], function() {
  return gulp.src([sourceAssets+'core/js/**/*.js', '!'+sourceAssets+'core/libs/**/*.js', '!'+sourceAssets+'core/js/essentials.js'])
    .pipe(babel())
    .on('error', console.error.bind(console))
    .pipe(preprocess({context: {version: version}}))
    .pipe(sourcemaps.init())
    .pipe(concat('core-dynamic.js'))
    .pipe(rename({suffix:'.min'}))
    .pipe(uglify())
    .pipe(sourcemaps.write('/maps'))
    .pipe(gulp.dest(buildAssets+'js/'))
});

// Cria o bundle de JS estático (vendors) do Core
gulp.task('create-core-static-js', function() {
  return gulp.src([sourceAssets+'core/libs/**/*.js'])
    .pipe(order([
        'babel/polyfill.js',
        'jquery/jquery-3.3.1.js',
        'jquery/jquery.mask.js',
        'jquery/jquery.validate.js',
        'jquery/jquery.validate-additional-methods.js',
        'jquery/jquery.typer.js',
        'foundation/*.js',
        '**/*.js'
    ]))
    .pipe(sourcemaps.init())
    .pipe(concat('core-static.js'))
    .pipe(rename({suffix:'.min'}))
    .pipe(uglify())
    .pipe(sourcemaps.write('/maps'))
    .pipe(gulp.dest(buildAssets+'js/'))
});

// Comprime todas as imagens do Core
gulp.task('compress-core-images', function() {
  return gulp.src(sourceAssets+'core/img/**/*')
    .pipe(cache(imagemin({
        optimizationLevel: 4,
        progressive: true,
        interlaced: true
    })))
    .pipe(gulp.dest(buildAssets+'img/'));
});

// Move todos os componentes HTML para a Build
gulp.task('move-all-components', function() {
  return gulp.src([sourcePath+'**/*.html'])
    .pipe(preprocess({context: {version: version}}))
    .pipe(gulp.dest(buildPath))
});

// Move demais arquivos das demais extensões não tratadas (fonts, audios, sourcemaps, etc)
gulp.task('move-all-other-files', function() {
  return gulp.src([sourcePath+'**/*.*',
    '!'+sourceAssets+'core/**/*.js',
    '!'+sourceAssets+'core/**/*.scss',
    '!'+sourceAssets+'core/**/*.css',
    '!'+sourceAssets+'core/img/*.*',
    '!'+sourceAssets+'core/libs/**/*.*',
    '!'+sourceAssets+'core/fonts/**/*.*',
    '!'+sourceAssets+'**/*.map',
    '!'+sourcePath+'**/*.html'])
    .pipe(gulp.dest(buildPath))
});

// Gera a build
gulp.task('process-build', [

    'create-core-dynamic-css',
    'create-core-static-css',

    'move-all-core-fonts',

    'create-core-dynamic-js',
    'create-core-static-js',

    'compress-core-images',

    'move-all-components',

    'move-all-other-files'
]);

// Cleans Build folder contents and deletes Temp Folder
gulp.task('clear', function(cb) {
    if (!watching) {
        del([buildPath.replace('/',''), finalBuildPath+'**/*']).then(function(res) {
            cb();
        });
    } else {
        cb();
    }
});

// Deletes Temp folder after completing Build Process
gulp.task('del-temp-build', ['build'] , function() {
    return del(buildPath);
});

// Moves all content present in Temp folder to Build, preventing project going down while running Gulp.
gulp.task('build', ['process-build'], function() {
    return gulp.src([buildPath+'**/*.*'])
    .pipe(gulp.dest(finalBuildPath, { overwrite:true }))
});

// Default Task (Build)
gulp.task('default', ['build','del-temp-build'], function() {
    browserSync.init({
        server: finalBuildPath
    });
});
